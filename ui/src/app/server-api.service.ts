import { Injectable, EventEmitter } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, Subscription } from 'rxjs';
import { IServer } from './server.model';

@Injectable({ providedIn: 'root' })
export class ServerApiService {
  private _url = 'http://localhost:4454/servers/';
  public updateView = new EventEmitter();
  public servers: IServer[] = [];

  constructor(private http: HttpClient) {}

  getAllServers(): Observable<IServer[]> {
    return this.http.get<IServer[]>(this._url);
  }

  getServer(server: IServer): Observable<IServer> {
    return this.http.get<IServer>(this._url + server.id);
  }

  turnServerOn(server: IServer): Subscription {
    switch (server.status) {
      case 'ONLINE':
        console.log(`Server with id ${server.id} is already turned on`);
        this.updateView.emit();
        break;

      case 'REBOOTING':
        console.log(`Server with id ${server.id} is rebooting`);
        this.updateView.emit();
        break;

      default:
        return this.http
          .put(this._url + server.id + '/on', { server })
          .subscribe(
            (data) => {
              console.log(data);
              this.updateView.emit();
            },
            (response) => {
              console.log(
                "'Turn server on' PUT call returned an error: ",
                response
              );
            },
            () => {
              console.log("'Turn server on' PUT call completed successfully");
            }
          );
    }
  }

  turnServerOff(server: IServer): Subscription {
    switch (server.status) {
      case 'OFFLINE':
        console.log(`Server with id ${server.id} is already turned off`);
        this.updateView.emit();
        break;

      case 'REBOOTING':
        console.log(`Server with id ${server.id} is rebooting`);
        this.updateView.emit();
        break;

      default:
        return this.http
          .put(this._url + server.id + '/off', { server })
          .subscribe(
            (data) => {
              console.log(data);
              this.updateView.emit();
            },
            (response) => {
              console.log(
                "PUT call 'Turn server off' returned an error: ",
                response
              );
            },
            () => {
              console.log("PUT call 'Turn server off' completed successfully");
            }
          );
    }
  }

  rebootServer(server: IServer): Subscription {
    switch (server.status) {
      case 'OFFLINE':
        console.log(
          `Server with id ${server.id} is offline and cannot be rebooted`
        );
        this.updateView.emit();
        break;

      case 'REBOOTING':
        console.log(`Server with id ${server.id} is already rebooting`);
        this.updateView.emit();
        break;

      default:
        return this.http
          .put(this._url + server.id + '/reboot', { server })
          .subscribe(
            (data: IServer) => {
              console.log(data);
              this.updateView.emit();
              let counter = 0;
              const apiCall = setInterval(() => {
                counter++;
                console.log('Server rebooting: ', counter);
                this.getServer(data).subscribe((updatedServer) => {
                  if (updatedServer.status === 'ONLINE') {
                    console.log(
                      'Server with id ' + server.id + ' online again'
                    );
                    this.updateView.emit();
                    clearInterval(apiCall);
                  }
                });
              }, 1000);
            },
            (response) => {
              console.log(
                "'Reboot server' PUT call returned an error: ",
                response
              );
            },
            () => {
              console.log("'Reboot server' PUT call completed successfully");
            }
          );
    }
  }
}
