import { Component, OnInit, Input } from '@angular/core';
import { ServerApiService } from 'src/app/server-api.service';
import { IServer } from 'src/app/server.model';

@Component({
  selector: 'app-select-menu',
  templateUrl: './select-menu.component.html',
  styleUrls: ['./select-menu.component.scss'],
})
export class SelectMenuComponent implements OnInit {
  statuses = [
    { value: 'on', viewValue: 'Turn On' },
    { value: 'off', viewValue: 'Turn Off' },
    { value: 'reboot', viewValue: 'Reboot' },
  ];

  @Input() currentServer: IServer;

  constructor(private serverApiService: ServerApiService) {}

  ngOnInit() {
    switch (this.currentServer.status) {
      case 'ONLINE':
        this.statuses = [
          { value: 'off', viewValue: 'Turn Off' },
          { value: 'reboot', viewValue: 'Reboot' },
        ];
        break;

      case 'OFFLINE':
        this.statuses = [{ value: 'on', viewValue: 'Turn On' }];
        break;

      default:
        this.statuses = [];
        break;
    }
  }

  onStatusChange(value) {
    switch (value) {
      case 'on':
        console.log('Turning on server with id ', this.currentServer.id);
        this.serverApiService.turnServerOn(this.currentServer);
        break;

      case 'off':
        console.log('Turning off server with id ', this.currentServer.id);
        this.serverApiService.turnServerOff(this.currentServer);
        break;

      default:
        console.log('Rebooting server with id ', this.currentServer.id);
        this.serverApiService.rebootServer(this.currentServer);
        break;
    }
  }
}
