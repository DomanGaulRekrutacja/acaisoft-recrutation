import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { ServerApiService } from '../server-api.service';
import { IServer } from '../server.model';
import { MatTableDataSource } from '@angular/material/table';

@Component({
  selector: 'app-servers',
  templateUrl: './servers.component.html',
  styleUrls: ['./servers.component.scss'],
})
export class ServersComponent implements OnInit {
  public serversData: IServer[] = [];
  serverQuantity = 0;

  displayedColumns: string[] = ['name', 'status', 'action'];
  dataSource = new MatTableDataSource(this.serversData);

  constructor(private serverApiService: ServerApiService) {}

  ngOnInit() {
    this.getServers();
    this.updateView();
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
    this.serverQuantity = this.dataSource.filteredData.length;
  }

  getServers() {
    this.serverApiService.getAllServers().subscribe((data) => {
      this.serversData = data;
      this.serverQuantity = this.serversData.length;
      this.dataSource = new MatTableDataSource(this.serversData);
    });
  }

  updateView() {
    this.serverApiService.updateView.subscribe(() => {
      setTimeout(() => {
        this.getServers();
      }, 50);
    });
  }
}
